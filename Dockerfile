# BASE stage
FROM node:10.16-alpine as base
LABEL maintainer="Moncy Gopalan"
WORKDIR /usr/src/app
RUN apk --no-cache add python make g++
COPY package.json ./
COPY yarn.lock ./
RUN yarn --prod &&\
  yarn cache clean

FROM node:10.16-alpine
WORKDIR /usr/src/app
ENV NODE_ENV production
COPY --from=base /usr/src/app/node_modules /usr/src/app/node_modules
COPY package.json ./
COPY yarn.lock ./
COPY ./index.js ./index.js

CMD ["yarn", "start"]
