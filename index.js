const AWS = require('aws-sdk');

const s3 = new AWS.S3();
const bucketName = process.env.BUCKET_NAME || 'bucket-name';

const fetchListOfObjects = async (bucketName) => {
  try {
    const response = await s3.listObjects({ Bucket: bucketName }).promise();
    console.log('response: ', response);
  } catch (error) {
    console.log('OOPS! ERROR: ', error);
  }
};

console.log('bucketName: ', bucketName);
fetchListOfObjects(bucketName);
